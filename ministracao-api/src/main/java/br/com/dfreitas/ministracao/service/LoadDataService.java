package br.com.dfreitas.ministracao.service;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dfreitas.ministracao.entity.Ministracao;
import br.com.dfreitas.ministracao.repository.MinistracaoRepository;

@Service
public class LoadDataService {

	@Autowired
	private MinistracaoRepository ministracaoRepository;

	public void loadData() {
		ministracaoRepository.deleteAll();

		// salvando alguns dados
		ministracaoRepository.save(new Ministracao("Aliança beneficio e responsabilidade",
				"Bp Daniel", "Ministério Verbo Vivo", null, "http://www.verbovivo.hospedagemdesites.ws/ouvir_mensagem_full.asp?id=711", null, null, LocalDateTime.now(), LocalDate.of(2019, 8, 4)));
											

		// buscando todas as minstrações
		System.out.println("Ministrações encontrados com findAll():");
		System.out.println("-------------------------------");
		for (Ministracao ministracao : ministracaoRepository.findAll()) {
			System.out.println(ministracao);
		}
		System.out.println();

	}

}
