package br.com.dfreitas.ministracao.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MinistracaoController {
	
	private final Logger logger = LoggerFactory.getLogger(MinistracaoController.class);
	
	@GetMapping("/ministracoes/{id}")
	public Object getMinistracao(String id) {
		logger.info(id);
		return "{'titulo': 'teste' }";
	}

}
