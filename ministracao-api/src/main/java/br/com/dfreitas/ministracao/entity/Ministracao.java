package br.com.dfreitas.ministracao.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;

public class Ministracao implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	
	private String titulo;
	
	private String preletor;
	
	private String ministerio;
		
	private String urlVideo;
	
	private String urlAudio;
	
	private String urlTexto;
	
	private String texto;
	
	private LocalDateTime dataCadastro;
	
	private LocalDate dataPublicacao;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getPreletor() {
		return preletor;
	}

	public void setPreletor(String preletor) {
		this.preletor = preletor;
	}

	public String getMinisterio() {
		return ministerio;
	}

	public void setMinisterio(String ministerio) {
		this.ministerio = ministerio;
	}

	public String getUrlVideo() {
		return urlVideo;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}

	public String getUrlAudio() {
		return urlAudio;
	}

	public void setUrlAudio(String urlAudio) {
		this.urlAudio = urlAudio;
	}

	public String getUrlTexto() {
		return urlTexto;
	}

	public void setUrlTexto(String urlTexto) {
		this.urlTexto = urlTexto;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalDate getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(LocalDate dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	@Override
	public String toString() {
		return "Ministracao [id=" + id + ", titulo=" + titulo + ", preletor=" + preletor + ", ministerio=" + ministerio
				+ ", urlVideo=" + urlVideo + ", urlAudio=" + urlAudio + ", urlTexto=" + urlTexto + ", texto=" + texto
				+ ", dataCadastro=" + dataCadastro + ", dataPublicacao=" + dataPublicacao + "]";
	}

	public Ministracao(String titulo, String preletor, String ministerio, String urlVideo, String urlAudio,
			String urlTexto, String texto, LocalDateTime dataCadastro, LocalDate dataPublicacao) {
		super();
		this.titulo = titulo;
		this.preletor = preletor;
		this.ministerio = ministerio;
		this.urlVideo = urlVideo;
		this.urlAudio = urlAudio;
		this.urlTexto = urlTexto;
		this.texto = texto;
		this.dataCadastro = dataCadastro;
		this.dataPublicacao = dataPublicacao;
	}

	public Ministracao() {
		super();
	}
	
	
	
	

}
