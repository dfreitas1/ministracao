package br.com.dfreitas.ministracao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.dfreitas.ministracao.service.LoadDataService;

@SpringBootApplication
public class MinistracaoApiApplication implements CommandLineRunner {
	@Autowired
	private LoadDataService loadDataService;

	public static void main(String[] args) {
		SpringApplication.run(MinistracaoApiApplication.class, args);
	}
	
	@Override
	  public void run(String... args) throws Exception {
		loadDataService.loadData();
	
	}



}
