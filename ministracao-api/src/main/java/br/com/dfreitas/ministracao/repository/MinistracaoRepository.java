package br.com.dfreitas.ministracao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.dfreitas.ministracao.entity.Ministracao;

public interface MinistracaoRepository extends MongoRepository<Ministracao, String> {

}
