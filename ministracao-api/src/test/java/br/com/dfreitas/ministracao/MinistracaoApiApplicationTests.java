package br.com.dfreitas.ministracao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles(value = "test")
class MinistracaoApiApplicationTests {

	@Test
	void contextLoads() {
		System.out.println("Testes ...");
	}

}
